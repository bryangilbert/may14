#!/usr/bin/env bash

echo Install Docker Compose
echo For complete details see the https://docs.docker.com/compose/install
echo The docs show how to install docker-compose on Mac, Windows, Linus, etc.

sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo docker-compose --version
