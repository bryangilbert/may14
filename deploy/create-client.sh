#!/usr/bin/env bash

project_name=$1

function usage() {
    echo "Usage $0 project_name"
    echo "This script creates a project that can use the Vue CLI. It then creates a new Vue client project"
    exit 1
}

if [[ -z "$project_name" ]]; then
    echo Required parameter project_name not set
    usage
fi

read -p "OK to proceed and create a new Vue project called: $project_name? [Yy]" -n 1 -r
echo    # move cursor to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
  echo Project Name: project_name
  mkdir $project_name
  cd $project_name
  npm init
  npm install --save  @vue/cli

  node node_modules/@vue/cli/bin/vue.js create $project_name
  cd $project_name
  npm run build
fi
