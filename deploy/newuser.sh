#!/usr/bin/env bash

new_user=$1

new_shell=bash

function usage() {
    echo "Usage $0 new_user_name"
    echo "This script creates a new user"
    exit 1
}
if [[ -z "$new_user" ]]; then
    echo Required parameter new_user not set
    usage
fi

read -p "Provide the users first password: " new_password
echo    # move cursor to a new line

read -p "Set user to have zsh shell ? [Y/N] " use_zsh
echo    # move cursor to a new line
new_shell=bash
if [[ $use_zsh == [yY] ]]; then new_shell=zsh; fi


echo User: $new_user
echo Password: $new_password
echo Shell: $new_shell

read -p "Continue? (Y/N): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1
echo "OK let's continue ..."

new_user_homedir=/home/"${new_user}"

addgroup ssh-access

if [[ "${new_shell}" == 'zsh' ]]; then

   apt-get install -y zsh

fi

echo Create user: "${new_user}" using password: "${new_password}"
# Create the user; add to group; create home directory (-m); set password hashed
useradd -G users,sudo,ssh-access -m -s /bin/"${new_shell}" -p $(echo "${new_password}" | openssl passwd -1 -stdin) "${new_user}"

echo Show information about the user:
getent passwd "${new_user}"
id -Gn "${new_user}"

echo "Copy public SSH keys installed by Digital Ocean in root into new users home directory"
cp -R /root/.ssh/ "${new_user_homedir}"

chmod 700 "${new_user_homedir}"/.ssh
chmod 600 "${new_user_homedir}"/.ssh/*
chown -R "${new_user}":"${new_user}" "${new_user_homedir}"/.ssh

exit
