#!/usr/bin/env bash

OLD_HOME=$HOME
SAMPLE_BASH=../resource/sample_bashrc
SAMPLE_ALIAS=../resource/custom_aliases
WD=$PWD

function runit() {
	HOME=$WD/$TMP
	cd ..
	./user_profile_alias.sh
	cd $WD
	HOME=$OLD_HOME
}

echo "TEST ONE"
TMP=tmp1
mkdir -p $TMP
rm ${TMP}/.b*
# just have a empty bashrc file
touch ${TMP}/.bashrc
runit

echo ""
echo "TEST TWO"
TMP=tmp2
mkdir -p $TMP
rm ${TMP}/.b*
cp $SAMPLE_BASH $TMP/.bashrc
runit

echo ""
echo "TEST THREE"
TMP=tmp3
mkdir -p $TMP
rm ${TMP}/.b*
cp $SAMPLE_BASH $TMP/.bashrc
cp $SAMPLE_ALIAS $TMP/
runit

HOME=$OLD_HOME
