#!/usr/bin/env bash

OLD_HOME=$HOME
SAMPLE_BASH=../resource/sample_bashrc
SC=user_profile_ssh_add.sh
WD=$PWD

function runit() {
	HOME=$WD/$TMP
	cd ..
	./$SC
	cd $WD
	HOME=$OLD_HOME
}

function setup() {
	mkdir -p ${TMP}
	rm ${TMP}/.b* 2> /dev/null
	rm ${TMP}/* 2> /dev/null
}
echo "TEST ONE"
TMP=tmp1
setup
# just have a empty bashrc file
touch ${TMP}/.bashrc
runit

echo ""
echo "TEST TWO"
TMP=tmp2
setup
cp $SAMPLE_BASH $TMP/.bashrc
runit

echo ""
echo "TEST THREE"
TMP=tmp3
setup
cp ../resource/sample_bashrc_w_ssh $TMP/.bashrc
runit


HOME=$OLD_HOME
